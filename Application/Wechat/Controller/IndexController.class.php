<?php
namespace Wechat\Controller;
use Think\Controller;
use Common\ORG\Wechat;
class IndexController extends Controller {

	private $wechat;
	private $data;

	function __construct() {
		$config = array(
			'token'        => '',
			'appid'        => '',
			'secret'       => '',
			'access_token' => '',
			'debug'        => true
		);
		$this->wechat = new Wechat($config);
		$this->wechat->valid();
		$this->data = $this->wechat->request();
	}

	/**
	 * 消息处理入口
	 */
	function index(){
		if (IS_POST) {
			switch ($this->data['msgtype']) {
				case 'event' :
					$this->event();
					break;
				default :
					$this->text();
					break;
			}
		}
	}

	/**
	 * 处理文本消息
	 */
	private function text() {
		$this->response($this->data['content']);
	}

	/**
	 * 处理用户事件
	 */
	private function event() {
		$event = $this->data['event'];
		switch ($event) {
			case 'subscribe': //关注公众号
				$this->response('欢迎关注C.Jango微信管理系统！');
				break;
			case 'unsubscribe': //取消关注公众号
				break;
			case 'CLICK' : //点击自定义菜单
				$this->response($this->data['eventkey'].'您点击了自定义菜单！');
				break;
			default:
				break;
		}
	}

	/**
	 * 重写的消息回复方法
	 */
	private function response($content) {
		$this->wechat->response($content);
	} 
}