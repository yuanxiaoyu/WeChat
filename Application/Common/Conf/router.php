<?php
return array(
	'URL_ROUTER_ON' => true,
	'URL_ROUTE_RULES'=>array(
			//静态页面部分
			'index$'          => 'Index/index',   //首页
			'intro'           => 'Index/page?action=intro',
			'about'           => 'Index/page?action=about',
			'price'           => 'Index/page?action=price',
			'help'            => 'Index/page?action=help',
			
			'register'        => 'User/register', //用户注册
			'login'           => 'User/login',    //用户登陆
			'logout'          => 'User/logout',   //退出登陆
			'u/:id\d$'        => 'User/index',    //用户展示
			'article/:id\d$'  => 'Article/detail',//文章详情
	)
);