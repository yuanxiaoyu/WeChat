<?php 
namespace Admin\Model;
use Think\Model;
class MemberLevelModel extends Model {
	
	protected $_auto = array(
			array('create_time', 'time', self::MODEL_INSERT, 'function'),
			array('update_time', 'time', self::MODEL_UPDATE, 'function')
	);
}