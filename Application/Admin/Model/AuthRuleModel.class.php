<?php
namespace Admin\Model;
use Think\Model;

class AuthRuleModel extends Model {
	protected $_validate = array(
			array('name','require','节点标识必须填写！'),
			array('title','require','节点名称必须填写！')
	);
	protected $_auto = array(
			array('status',1,1)
	);
	function lists() {
		$map['status'] = IS_ROOT?array('egt',-1):array('egt',0);
		return $this->where($map)->order('sort asc')->select();
	}
	function info($id) {
		if (is_numeric($id)) {
			$map['id'] = $id;
		}else {
			$map['name'] = $id;
		}
		$map['status'] = IS_ROOT?array('egt',-1):array('egt',0);
		return $this->where($map)->find();
	}
	function getPath($id){
		$path = array();
		$nav = $this->where("id={$id}")->field('id,pid,title')->find();
		$path[] = $nav;
		if($nav['pid'] >1){
			$path = array_merge($this->getPath($nav['pid']),$path);
		}
		return $path;
	}
}