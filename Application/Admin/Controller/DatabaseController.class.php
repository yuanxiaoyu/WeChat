<?php
namespace Admin\Controller;

class DatabaseController extends AdminController {
	
	function export($name = '') {
		if (IS_POST) {
			if (empty($name)) $this->err('请选择至少一个表！');
			$name = str2arr($name, ',');
			$model = D('Database');
			if ($model->backup($name)) {
				$this->ok('数据库备份成功！');
			}else {
				$this->err($model->getError());
			}
		}else {
			$list = D('Database')->lists();
			$this->assign('__list__',$list);
			$this->display();
		}
	}
	
	function optimize() {
		$name = I('request.name');
		empty($name) && $this->err('请指定要优化的表！');
		$db   = \Think\Db::getInstance();
		$name = str2arr($name, ',');
		foreach ($name as $k=>$v) {
			$table .= "`".$v."`,";
		}
		$table = rtrim($table, ',');
		$list = $db->query("OPTIMIZE TABLE ".$table);
		if($list){
			$this->ok("数据表优化完成！");
		} else {
			$this->err("数据表优化出错请重试！");
		}
	}
	
	function repair() {
		$name = I('request.name');
		empty($name) && $this->err('请指定要优化的表！');
		$db   = \Think\Db::getInstance();
		$name = str2arr($name, ',');
		foreach ($name as $k=>$v) {
			$table .= "`".$v."`,";
		}
		$table = rtrim($table, ',');
		$list = $db->query("REPAIR TABLE ".$table);
		if($list){
			$this->ok("数据表修复完成！");
		} else {
			$this->err("数据表修复出错请重试！");
		}
	}
	
	function import() {
		$directory = C('DATA_BACKUP_PATH');
		if (IS_POST) {
			$this->ok();
		}else {
			$files = array_slice(@scandir($directory), 2);
			$this->assign('__list__', $files);
			$this->display();
		}
	}
}