<?php
namespace Admin\Controller;

class WechatController extends AdminController {
	
	function setting() {
		
	}
	
	function attention() {
		$this->display();
	}
	
	function reply() {
		$keyword = I('post.keyword');
		if (empty($keyword)) {
			$map = '';
		}else {
			$map['keyword'] = array('LIKE', "%{$keyword}%");
		}
		$this->lists('Reply' ,$map);
		$this->display();
	}
	
	function reply_add() {
		$this->display('reply_edit');
	}
	
	function reply_edit() {
		$this->display('reply_edit');
	}
	
	function reply_del() {
		$id = I('request.id');
		empty($id) && $this->err('至少选择一条数据！');
		$map['id'] = array('IN', $id);
		if (M('Reply')->where($map)->delete()) {
			$this->ok('删除成功');
		}else {
			$this->err('删除失败');
		}
	}
	
	function user() {
		
	}
}