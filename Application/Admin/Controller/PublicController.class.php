<?php
namespace Admin\Controller;
use Think\Controller;
use User\Api\UserApi;
class PublicController extends Controller {
	
	/**
	 * 用户登陆
	 */
	function login($username = null, $password = null) {
		/* 读取数据库中的配置 */
		$config	=	S('DB_CONFIG_DATA');
		if(!$config){
			$config	=	D('Config')->lists();
			S('DB_CONFIG_DATA',$config);
		}
		C($config); //添加配置
		if(IS_POST){
			/* 调用UC登录接口登录 */
			$User = new UserApi;
			$uid = $User->login($username, $password);
			if(0 < $uid){ //UC登录成功
				/* 登录用户 */
				$Member = D('Member');
				if($Member->login($uid)){ //登录用户
					$init_page = cookie('__init_page__');
					cookie('__init_page__', null);
					$url = !empty($init_page)?$init_page:U('/Admin');
					$this->success('登录成功！', $url);
				} else {
					$this->error($Member->getError());
				}
		
			} else { //登录失败
				switch($uid) {
					case -1: $error = '用户不存在或被禁用！'; break; //系统级别禁用
					case -2: $error = '密码错误！'; break;
					default: $error = '未知错误！'; break; // 0-接口参数错误（调试阶段使用）
				}
				$this->error($error);
			}
		} else {
			if(is_login()){
				$this->redirect('Index/index');
			}else{
				$this->meta_title = '管理登陆';
				$this->display();
			}
		}
	}
	
	/**
	 * 退出登录
	 */
	public function logout(){
		if(is_login()){
			D('Member')->logout();
			session('[destroy]');
			$this->success('退出成功！', U('login'));
		} else {
			$this->redirect('login');
		}
	}
	
	/**
	 * 验证码
	 */
	public function verify(){
		$verify = new \Think\Verify();
		$verify->entry(1);
	}
}