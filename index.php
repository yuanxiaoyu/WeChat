<?php
define('APP_DEBUG',True);
// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');
// 版本号
define('SYS_VERSION', '1.0.0');
// 定义缓存目录
define('RUNTIME_PATH', './Runtime/');
// 定义应用目录
define('APP_PATH','./Application/');
// 不需要生成目录安全文件
define('BUILD_DIR_SECURE', false);
// 引入ThinkPHP入口文件
require './ThinkPHP/ThinkPHP.php';