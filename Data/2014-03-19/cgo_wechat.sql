-- -----------------------------
-- Table structure for `cgo_wechat`
-- Backup date 2014-03-19 10:13:31
-- -----------------------------
DROP TABLE IF EXISTS `cgo_wechat`;
CREATE TABLE IF NOT EXISTS `cgo_wechat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `wxid` varchar(20) NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

