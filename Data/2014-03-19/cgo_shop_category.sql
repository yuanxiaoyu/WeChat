-- -----------------------------
-- Table structure for `cgo_shop_category`
-- Backup date 2014-03-19 10:13:31
-- -----------------------------
DROP TABLE IF EXISTS `cgo_shop_category`;
CREATE TABLE IF NOT EXISTS `cgo_shop_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL,
  `sort` tinyint(2) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `tpl_index` varchar(50) NOT NULL,
  `tpl_lists` varchar(50) NOT NULL,
  `tpl_detail` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

