-- -----------------------------
-- Table structure for `cgo_reply`
-- Backup date 2014-03-19 10:13:31
-- -----------------------------
DROP TABLE IF EXISTS `cgo_reply`;
CREATE TABLE IF NOT EXISTS `cgo_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `type` varchar(10) NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `keyword` (`keyword`),
  KEY `appid_2` (`keyword`),
  KEY `appid_3` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

